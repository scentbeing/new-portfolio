/** @jsx jsx */
import { Box, Link, Flex, jsx, useColorMode } from "theme-ui"

const Footer = () => {
  const [colorMode] = useColorMode()
  const isDark = colorMode === `dark`

  return (
    <div></div>
    // <Box as="footer" variant="layout.footer">
    //   Copyright &copy; {new Date().getFullYear()}. All rights reserved.
    // </Box>
  )
}

export default Footer
