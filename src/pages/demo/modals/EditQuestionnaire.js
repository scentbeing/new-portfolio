import React, { useState, useEffect } from 'react'
import { Grid, Card, Text, Heading, Button, Label, Input, Textarea, Radio } from 'theme-ui'
import Modal from 'react-modal';

const customStyles = {
  overlay: {
    background: "rgba(33, 36, 40, 0.91)"
  },
  content: {
    width: "80%",
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: "var(--theme-ui-colors-background)"
  },
}


// Make sure to bind modal to your appElement (https://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#___gatsby');

function EditQuestionnaire({ isOpened, onOpened, resetOnClose, onClose, onSubmit, question, answer1, answer2, answer3, answer4, answerSelected }) {

  const data = {
    question,
    answer1,
    answer2,
    answer3,
    answer4,
    answerSelected
  }

  const [modalIsOpen, setIsOpen] = useState(false)
  const [answerSelectedSelect, setAnswerSelectedSelect] = useState(answerSelected || "answer1")

  const [questionInput, setQuestionInput] = useState(question || "")
  const [answer1Input, setAnswer1Input] = useState(answer1 || "")
  const [answer2Input, setAnswer2Input] = useState(answer2 || "")
  const [answer3Input, setAnswer3Input] = useState(answer3 || "")
  const [answer4Input, setAnswer4Input] = useState(answer4 || "")

  useEffect(() => {
    setIsOpen(isOpened)
  }, [isOpened])

  function resetData() {
    setQuestionInput(data.question)
    setAnswer1Input(data.answer1)
    setAnswer2Input(data.answer2)
    setAnswer3Input(data.answer3)
    setAnswer4Input(data.answer4)
    setAnswerSelectedSelect(data.answerSelected)

  }

  function openModal() {
    setIsOpen(true);
    if (onOpened) onOpened()

  }

  function handleSubmit() {
    onSubmit({
      question: questionInput,
      answer1: answer1Input,
      answer2: answer2Input,
      answer3: answer3Input,
      answer4: answer4Input,
      answerSelected: answerSelectedSelect,
    })


    data.question = questionInput;
    data.answer1 = answer1Input;
    data.answer2 = answer2Input;
    data.answer3 = answer3Input;
    data.answer4 = answer4Input;
    data.answerSelected = answerSelectedSelect;

    // resetForm()
    closeModal()
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    // subtitle.style.color = '#f00';
  }

  function closeModal() {
    setIsOpen(false);
    // resetForm();
    if (resetOnClose) {
      resetForm();
    } else {
      resetData();
    }
    if (onClose) onClose()
  }

  function fill() {
    setQuestionInput("Nunc vel erat finibus, pellentesque nulla sit amet, ultricies eros?")
    setAnswer1Input("Pellentesque rutrum vulputate euismod.")
    setAnswer2Input("Phasellus nec pellentesque augue.")
    setAnswer3Input("In et orci mauris.")
    setAnswer4Input("Class aptent taciti sociosqu ad lito")
    setAnswerSelectedSelect("answer1")
  }

  function resetForm() {
    setQuestionInput("")
    setAnswer1Input("")
    setAnswer2Input("")
    setAnswer3Input("")
    setAnswer4Input("")
    setAnswerSelectedSelect("answer1")

  }


  return (
    <div>
      {/* <button onClick={openModal}>Open Modal</button> */}
      <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <Button className={`float-right close-button`} onClick={closeModal}><strong>X</strong></Button>

        <Heading as={"h1"} mb={0}>
          Questionnaire
        </Heading>
        <p className={`underline pointer`} onClick={fill}>Fill out form - I don't want to type</p>
        <Label htmlFor="question">Question</Label>
        <Input name="question" id="question" mb={3} value={questionInput} onChange={e => setQuestionInput(e.target.value)} />
        <br />

        <Label htmlFor="answer1">Answer 1</Label>
        <Input name="answer1" id="answer1" mb={3} value={answer1Input} onChange={e => setAnswer1Input(e.target.value)} />
        <Label className={`pointer`}>
          <Radio
            name='is-answer-correct'
            value='answer1'
            checked={answerSelectedSelect === "answer1"}
            onChange={e => setAnswerSelectedSelect(e.target.value)}
          />
          correct answer
        </Label>
        <br />

        <Label htmlFor="answer2">Answer 2</Label>
        <Input name="answer2" id="answer2" mb={3} value={answer2Input} onChange={e => setAnswer2Input(e.target.value)} />
        <Label className={`pointer`}>
          <Radio
            name='is-answer-correct'
            value='answer2'
            checked={answerSelectedSelect === "answer2"}
            onChange={e => setAnswerSelectedSelect(e.target.value)}
          />
          correct answer
        </Label>
        <br />

        <Label htmlFor="answer3">Answer 3</Label>
        <Input name="answer3" id="answer3" mb={3} value={answer3Input} onChange={e => setAnswer3Input(e.target.value)} />
        <Label className={`pointer`}>
          <Radio
            name='is-answer-correct'
            value='answer3'
            checked={answerSelectedSelect === "answer3"}
            onChange={e => setAnswerSelectedSelect(e.target.value)}
          />
          correct answer
        </Label>
        <br />

        <Label htmlFor="answer4">Answer 4</Label>
        <Input name="answer4" id="answer4" mb={3} value={answer4Input} onChange={e => setAnswer4Input(e.target.value)} />
        <Label className={`pointer`}>
          <Radio
            name='is-answer-correct'
            value='answer4'
            checked={answerSelectedSelect === "answer4"}
            onChange={e => setAnswerSelectedSelect(e.target.value)}
          />
          correct answer
        </Label>
        <br />

        {/* <Button  onClick={closeModal}>cancel</Button> */}
        <Button className={`pointer`} variant={"secondary"} onClick={handleSubmit}>SAVE</Button>


      </Modal>

    </div>
  )
}

export default EditQuestionnaire
