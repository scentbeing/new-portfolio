import React, { useState, useEffect } from 'react'
import Modal from 'react-modal';
import { Grid, Card, Text, Heading, Button, Label, Input, Textarea, Radio } from 'theme-ui'

// Make sure to bind modal to your appElement (https://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#___gatsby');

const customStyles = {
	content: {
		width: "80%",
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		background: "var(--theme-ui-colors-background)"
	},
}

function DeleteQuestionnaire({ isOpened, onOpened, onClose, onSubmit, question }) {

	const [modalIsOpen, setIsOpen] = useState(false);

	useEffect(() => {
		setIsOpen(isOpened)
	}, [isOpened])

	function openModal() {
		setIsOpen(true);
		if (onOpened) onOpened()
	}

	function afterOpenModal() {
		// references are now sync'd and can be accessed.
		// subtitle.style.color = '#f00';
	}

	function closeModal() {
		setIsOpen(false);
		if (onClose) onClose()
	}

	function handleSubmit() {
		setIsOpen(false);
		if (onSubmit) onSubmit()
	}

	return (
		<div>
			{/* <button onClick={openModal}>Open Modal</button> */}
			<Modal
				isOpen={modalIsOpen}
				onAfterOpen={afterOpenModal}
				onRequestClose={closeModal}
				style={customStyles}
				contentLabel="Example Modal"
			>
				<Button className={`float-right close-button`} onClick={closeModal}><strong>X</strong></Button>

				<Heading as={"h2"} mb={3}>Delete Question</Heading>

				<p>Are you sure you want to delete the following question?</p>

				<p><strong>{question}</strong></p>

				<Button className={`close-button`} onClick={handleSubmit}><strong>Delete Question</strong></Button>
			</Modal>
		</div>
	)
}

export default DeleteQuestionnaire
