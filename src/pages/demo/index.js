import * as React from "react"
import { Link, navigate, graphql } from "gatsby"
import { Container, Themed, Link as TLink } from "theme-ui"
import Layout from "@lekoarts/gatsby-theme-emma/src/components/layout"
import Seo from "@lekoarts/gatsby-theme-emma/src/components/seo"
import ProjectItem from "@lekoarts/gatsby-theme-emma/src/components/project-item"

import { Grid, Box, Card, Text, Heading, Button } from 'theme-ui'
import DeleteQuiz from "./modals/DeleteQuiz.js"



const Index = ({data}) => {

  const [quizes, setQuizes] = React.useState(null)
  const [editModesOpened, setEditModesOpened] = React.useState(null)

  React.useEffect(() => {
    const quizes = JSON.parse(localStorage.getItem("quizes"))

    setQuizes(quizes ? quizes.quizes : [])
  }, [])

  React.useEffect(() => {
    if (quizes) {
      setEditModesOpened(new Array(quizes.length).fill(false))
    }
  }, [quizes])

  const handleDeleteClose = () => {
    setEditModesOpened(new Array(quizes.length).fill(false))
  }

  const openModal = (e, index) => {
    e.preventDefault()
    const newEditModesOpened = [...editModesOpened]
    newEditModesOpened[index] = true;
    setEditModesOpened(newEditModesOpened)
  }

  const handleQuizDelete = (index) => {
    const newQuizes = [...quizes]
    newQuizes.splice(index, 1)

    setQuizes(newQuizes)

    localStorage.setItem("quizes", JSON.stringify({ quizes: newQuizes }))

    setEditModesOpened(new Array(quizes.length).fill(false))
  }

  return (
    <Layout>
      <Seo title="Demo Quiz App" />
      <Container>
        <Themed.p>
          <Button className={`primary-button`} sx={{ marginTop: "28px", float: "right", variant: 'buttons.toggle', fontSize: "1rem" }} onClick={() => navigate("/demo/create")}>Create Quiz</Button>
          <TLink as={Link} to="/" className={`underline`}>
            Back to Portfolio
          </TLink>
          <Heading as={"h1"} mb={5}>Quiz App</Heading>

        </Themed.p>
        <Grid
          columns={["1fr", '1fr 1fr', 2]}>
          <Box
            className={` primary-bg quiz-card`}
            padding={[2, 4, 4]} >
            <Card>
              <Heading as={"h2"} className={`quiz-name`}>{data.quizJson.name}</Heading>
              <Text className={`quiz-description`}>{data.quizJson.description}</Text>
              <br />
              <br />
              <Button onClick={() => navigate(`/demo/quiz?id=programing`)} className={`primary-button full-width`}>Start</Button>

            </Card>
          </Box>

          {editModesOpened && quizes.map((quiz, i) => (
            <Box
              key={quiz.id}
              className={`quiz-card`}
              padding={[2, 4, 4]} >
              <DeleteQuiz
                isOpened={editModesOpened[i]}
                onClose={handleDeleteClose}
                onSubmit={() => handleQuizDelete(i)}
                question={quiz.name}

              />
              <Card>
                <Button className={`float-right close-button`} onClick={e => openModal(e, i)}><strong>X</strong></Button>

                <Heading as={"h2"} className={`quiz-name`}>{quiz.name}</Heading>
                <Text className={`quiz-description`}>{quiz.description}</Text>
                <br />
                <br />
                <Button onClick={() => navigate(`/demo/quiz?id=${quiz.id}`)} className={`primary-button full-width`}>Start</Button>

              </Card>
            </Box>
          ))}
        </Grid>
      </Container>
    </Layout>
  )
}

export const query = graphql`
query {
  quizJson {
    name
    description
    questions {
      question
      answer1
      answer2
      answer3
      answer4
      answerSelected
    }
  }
}
`

export default Index
