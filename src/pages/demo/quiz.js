import React, { useState, useEffect } from "react"
import { Link, navigate, graphql } from "gatsby"
import { Container, Themed, Link as TLink } from "theme-ui"
import Layout from "@lekoarts/gatsby-theme-emma/src/components/layout"
import Seo from "@lekoarts/gatsby-theme-emma/src/components/seo"
import ProjectItem from "@lekoarts/gatsby-theme-emma/src/components/project-item"

import { Grid, Box, Card, Text, Heading, Button, Radio, Label } from 'theme-ui'

const Quiz = ({ location, data }) => {

  const [questionNumber, setQuestionNumber] = useState(0)
  const [question, setQuestion] = useState("")
  const [answer1, setAnswer1] = useState("")
  const [answer2, setAnswer2] = useState("")
  const [answer3, setAnswer3] = useState("")
  const [answer4, setAnswer4] = useState("")
  const [answerSelected, setAnswerSelected] = useState("")

  const [answerSelectedSelect, setAnswerSelectedSelect] = useState(null);

  // answer1, answer2, answer3, answer4
  const [correctAnswer, setCorrectAnswer] = useState(null);

  // answer1, answer2, answer3, answer4
  const [wrongAnswer, setWrongAnswer] = useState(null);

  const [correctAnswerCount, setCorrectAnswerCount] = useState(0);

  const [quiz, setQuiz] = useState(null)

  // "CHECK", "NEXT", "RESULTS"
  const [mainButton, setMainButton] = useState("CHECK")
  // QUIZ, RESULTS
  const [view, setView] = useState("QUIZ")

  useEffect(() => {
    const allQuizes = localStorage.getItem("quizes") ? JSON.parse(localStorage.getItem("quizes")).quizes : [];

    const params = new URLSearchParams(location.search);
    const parameterId = params.get("id");

    let selectedQuiz
    if (parameterId === "programing") {
      selectedQuiz = data.quizJson
    } else {
      selectedQuiz = allQuizes.filter(q => {

        return q.id.toString() === parameterId.toString()

      })[0]
    }

    setQuiz(selectedQuiz)
  }, [])

  useEffect(() => {
    if (quiz) {
      loadQuestion(questionNumber)
    }
  }, [quiz, questionNumber])


  const loadQuestion = (questionArrayIndex) => {
    const targetQuestion = quiz.questions[questionArrayIndex]
    setQuestion(targetQuestion.question)
    setAnswer1(targetQuestion.answer1)
    setAnswer2(targetQuestion.answer2)
    setAnswer3(targetQuestion.answer3)
    setAnswer4(targetQuestion.answer4)
    setAnswerSelected(targetQuestion.answerSelected)
  }

  const grade = () => {
    setCorrectAnswer(answerSelected)

    if (answerSelected !== answerSelectedSelect) {
      setWrongAnswer(answerSelectedSelect)
    } else {
      setCorrectAnswerCount(correctAnswerCount + 1)
    }

  }

  const handleMainButton = () => {
    //end of test results:
    if (mainButton === "CHECK") {
      grade()

      if (questionNumber + 1 === quiz.questions.length) {
        setMainButton("RESULTS")
      } else {
        setMainButton("NEXT")
      }
    }

    if (mainButton === "NEXT") {
      setWrongAnswer(null)
      setCorrectAnswer(null)
      setAnswerSelectedSelect(null)
      setQuestionNumber(questionNumber + 1)
      setMainButton("CHECK")

    }
    if (mainButton === "RESULTS") {
      setView("RESULTS")

    }

  }


  return (
    <Layout>
      <Seo title="Demo Quiz App" />
      <Container>

        <Themed.p>
          <TLink as={Link} to="/demo" className={`underline`}>
            Back to Quiz Selection
          </TLink>
          <Heading as={"h1"} mb={0}>Quiz</Heading>
          {quiz && (<p>{quiz.name}</p>)}

        </Themed.p>
        <br /><br />
        {view === "QUIZ" && (

          <Card>

            {quiz && (<p>Question: {questionNumber + 1} out of {quiz.questions.length}</p>)}
            <Heading as={"h3"}>{question}</Heading>
            <br />

            <table className={`table-demo`}>
              <tbody>
                <tr className={`pointer answer-hover`} onClick={e => setAnswerSelectedSelect("answer1")}>
                  <td className={`radio-td`}>
                    <Label className={`pointer`}>
                      <Radio
                        name='answer'
                        value='answer1'
                        checked={answerSelectedSelect === "answer1"}
                      />
                    </Label>
                  </td>
                  <td className={`${correctAnswer === "answer1" ? "background-right" : ""} ${wrongAnswer === "answer1" ? "background-wrong" : ""}`}>
                    {answer1}

                  </td>
                </tr>
                <tr className={`pointer answer-hover`} onClick={e => setAnswerSelectedSelect("answer2")}>
                  <td className={`radio-td`}>
                    <Label className={`pointer`}>
                      <Radio
                        name='answer'
                        value='answer2'
                        checked={answerSelectedSelect === "answer2"}
                      />
                    </Label>
                  </td>
                  <td className={`${correctAnswer === "answer2" ? "background-right" : ""} ${wrongAnswer === "answer2" ? "background-wrong" : ""}`}>
                    {answer2}

                  </td>
                </tr>
                <tr className={`pointer answer-hover`} onClick={e => setAnswerSelectedSelect("answer3")}>
                  <td className={`radio-td`}>
                    <Label className={`pointer`}>
                      <Radio
                        name='answer'
                        value='answer3'
                        checked={answerSelectedSelect === "answer3"}
                      />
                    </Label>
                  </td>
                  <td className={`${correctAnswer === "answer3" ? "background-right" : ""} ${wrongAnswer === "answer3" ? "background-wrong" : ""}`}>
                    {answer3}

                  </td>
                </tr>
                <tr className={`pointer answer-hover`}

                  onClick={e => setAnswerSelectedSelect("answer4")}>
                  <td className={`radio-td`}>
                    <Label className={`pointer`}>
                      <Radio
                        name='answer'
                        value='answer4'
                        checked={answerSelectedSelect === "answer4"}
                      />
                    </Label>
                  </td>
                  <td className={`${correctAnswer === "answer4" ? "background-right" : ""} ${wrongAnswer === "answer4" ? "background-wrong" : ""}`}>
                    {answer4}

                  </td>
                </tr>
              </tbody>
            </table>
            <br />
            <br />
            {answerSelectedSelect !== null && (<div className={`primary-button pointer`} onClick={handleMainButton}>{mainButton}</div>)}
          </Card>

        )}
        {view === "RESULTS" && (

          <Card>
            {/* <Heading as={"h3"}>"{quiz.name}" Completed!</Heading> */}

            {/* <br /> */}

            {correctAnswerCount} out of {quiz.questions.length} correct.

            <br />
            <br />
            <br />

            <div className={`primary-button pointer`} onClick={() => window.location.reload()}>Retry Quiz</div>
            <div className={`primary-button pointer`} onClick={() => navigate("/demo")}>Quiz Selection</div>
            <div className={`primary-button pointer`} onClick={() => navigate("/")}>Back to Portfolio</div>
          </Card>

        )}

      </Container>
    </Layout>
  )
}

export const query = graphql`
query {
  quizJson {
    name
    description
    questions {
      question
      answer1
      answer2
      answer3
      answer4
      answerSelected
    }
  }
}
`

export default Quiz
