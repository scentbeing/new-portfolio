import * as React from "react"
import { Link, navigate } from "gatsby"
import { Container, Themed, Link as TLink } from "theme-ui"
import Layout from "@lekoarts/gatsby-theme-emma/src/components/layout"
import Seo from "@lekoarts/gatsby-theme-emma/src/components/seo"
import ProjectItem from "@lekoarts/gatsby-theme-emma/src/components/project-item"
import Theme from "@lekoarts/gatsby-theme-emma/src/gatsby-plugin-theme-ui"

import { Grid, Card, Text, Heading, Button, Label, Input, Textarea, Radio } from 'theme-ui'
import DeleteQuestionnaire from "./modals/DeleteQuestionnaire"
import EditQuestionnaire from "./modals/EditQuestionnaire"


const quiz = function ({ name, description, questions }) {
  this.id = Date.now()
  this.name = name;
  this.description = description;
  this.questions = questions || [];
}

const questionSet = function ({ question, answer1, answer2, answer3, answer4, answerSelected }) {
  this.id = Date.now() + Math.random()
  this.question = question;
  this.answer1 = answer1;
  this.answer2 = answer2;
  this.answer3 = answer3;
  this.answer4 = answer4;
  this.answerSelected = answerSelected
}


const Main_CreateQuiz = () => {
  const [isCreateModalOpened, setIsCreateModalOpened] = React.useState(false);
  const [quizName, setQuizName] = React.useState("");
  const [quizDescription, setQuizDescription] = React.useState("");
  const [questionBeingEditedIndex, setQuestionBeingEditedIndex] = React.useState(null)

  const [questions, setQuestions] = React.useState([])
  const [editModesOpened, setEditModesOpened] = React.useState([])
  const [deleteModesOpened, setDeleteModesOpened] = React.useState([])

  React.useEffect(() => {
    setEditModesOpened(new Array(questions.length).fill(false))
    setDeleteModesOpened(new Array(questions.length).fill(false))

  }, [questions])

  const fill = () => {
    setQuizName(" Vivamus id mauris a mi.")

    setQuizDescription("Aliquam lacinia sapien porta pulvinar consectetur.")

    setQuestions([
      new questionSet({
        question: "Nunc vel erat finibus, pellentesque nulla sit amet, ultricies eros?",
        answer1: "Pellentesque rutrum vulputate euismod.",
        answer2: "Phasellus nec pellentesque augue.",
        answer3: "In et orci mauris.",
        answer4: "Class aptent taciti sociosqu ad lito.",
        answerSelected: "answer1"
      }),
      new questionSet({
        question: "Vivamus congue dignissim eros, eu iaculis diam pellentesque non?",
        answer1: "Phasellus nec pellentesque augue.",
        answer2: "Pellentesque rutrum vulputate euismod.",
        answer3: "Class aptent taciti sociosqu ad lito.",
        answer4: "In et orci mauris.",
        answerSelected: "answer2"
      }),

    ])
  }

  const handleQuestionCreate = () => {
    setQuestionBeingEditedIndex(null)
    setIsCreateModalOpened(true)
  }


  const handleQuestionnaireClose = (index) => {
    setEditModesOpened(new Array(questions.length).fill(false))
    setIsCreateModalOpened(false)
  }

  const handleDeleteClose = (index) => {
    setDeleteModesOpened(new Array(questions.length).fill(false))
  }

  const handleQuestionnaireSave = (newQuestion) => {
    const newQuestions = [...questions];

    if (!isCreateModalOpened) {
      newQuestions[questionBeingEditedIndex] = newQuestion;
    } else {
      newQuestions.unshift(new questionSet(newQuestion))
    }

    setQuestions(newQuestions)

    setIsCreateModalOpened(false)
  }

  const handleQuestionEdit = (index) => {
    const newEditModesOpened = [...editModesOpened]
    newEditModesOpened[index] = true;
    setEditModesOpened(newEditModesOpened)

    setQuestionBeingEditedIndex(index)
    // setIsCreateModalOpened(true)
  }

  const handleDeleteOpen = (index) => {
    const newDeleteModesOpened = [...deleteModesOpened]
    newDeleteModesOpened[index] = true;
    setDeleteModesOpened(newDeleteModesOpened)
  }

  const handleSave = () => {
    const newQuiz = new quiz({
      name: quizName,
      description: quizDescription,
      questions: questions
    })

    let quizes
    const savedQuizes = localStorage.getItem("quizes")

    if (savedQuizes) {
      quizes = JSON.parse(savedQuizes)
    } else {
      quizes = { quizes: [] }
    }

    quizes.quizes.unshift(newQuiz)

    localStorage.setItem("quizes", JSON.stringify(quizes))

    navigate(`/demo/quiz?id=${newQuiz.id}`)
  }

  const handleQuestionDelete = (index) => {
    const newQuestions = [...questions];
    newQuestions.splice(index, 1)

    setQuestions(newQuestions);
  }

  return (
    <Layout>
      <Seo title="Demo Quiz App" />
      <Container>
        <Themed.p>
          <TLink className={`underline`} as={Link} to="/demo">
            Back to Quiz Selection
          </TLink>
          <Heading as={"h1"} mb={0}>Create a Quiz</Heading>
          <p className={`underline pointer`} style={{ marginBottom: "3.6rem" }} onClick={fill}>Fill out form - I don't want to type</p>

        </Themed.p>

        <Label htmlFor="quiz_name">Quiz Name</Label>
        <Input name="quiz_name" id="quiz_name" value={quizName} onChange={e => setQuizName(e.target.value)} mb={3} />

        <Label htmlFor="quiz_description">Description</Label>
        <Textarea name="quiz_description" id="quiz_description" value={quizDescription} onChange={e => setQuizDescription(e.target.value)} rows={6} mb={3} />
        <br />
        <br />
        <table className={`table-demo`}>
          <thead>
            <tr>
              <th>Questions</th>
              <th style={{ width: "100px" }}>
                <p
                  style={{ padding: 0, margin: 0, color: "var(--theme-ui-colors-green-3)", cursor: "pointer", textDecoration: "underline" }}
                  onClick={handleQuestionCreate}
                >

                  Create
                </p>
              </th>
            </tr>
          </thead>
          <tbody>
            {questions.length === 0 && (
              <tr>
                <td colSpan="2">
                  <center>
                    No questions created yet.
                  </center>
                </td>
              </tr>
            )}
            {questions.map((q, i) => (
              <tr key={q.id}>
                <td>{q.question}</td>
                <td style={{ textAlign: "right" }}>
                  <p className={`no-spacing edit-text`} onClick={() => handleQuestionEdit(i)}>Edit</p>
                  {/* <p className={`no-spacing edit-text`} onClick={() => editModesOpened[i]()}>Edit</p> */}
                  {/* editModesOpened */}
                  <EditQuestionnaire
                    isOpened={editModesOpened[i]}
                    onClose={handleQuestionnaireClose}
                    // onOpened={() => setIsEditModalOpened(true)}
                    onSubmit={handleQuestionnaireSave}
                    question={q.question}
                    answer1={q.answer1}
                    answer2={q.answer2}
                    answer3={q.answer3}
                    answer4={q.answer4}
                    answerSelected={q.answerSelected}

                  />
                  <p className={`no-spacing delete-text`} onClick={() => handleDeleteOpen(i)}>Delete</p>
                  {/* <p className={`no-spacing delete-text`} onClick={() => setIsDeleteModalOpened(true)}>Delete</p> */}


                  <DeleteQuestionnaire
                    isOpened={deleteModesOpened[i]}
                    // onClose={() => setIsDeleteModalOpened(false)}
                    onClose={handleDeleteClose}
                    // onOpened={() => setIsDeleteModalOpened(true)}
                    onSubmit={() => handleQuestionDelete(i)}
                    question={q.question}

                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <br />
        {questions.length > 0 && (
          <Button
            className={`primary-button`}
            onClick={handleSave}
          >
            Save
          </Button>
        )}


        <div>
          <EditQuestionnaire
            isOpened={isCreateModalOpened}
            onClose={handleQuestionnaireClose}
            resetOnClose={true}
            // onOpened={() => setIsEditModalOpened(true)}
            onSubmit={handleQuestionnaireSave}

          />
        </div>
      </Container>
    </Layout >
  )
}

export default Main_CreateQuiz
